let express = require("express");
let path = require("path");
let app = require('./src/backend/server.js');
let http = require('http');
let fs = require("fs");

let port = process.env.PORT || '4200';
app.set('port', port);

app.use(express.static(path.join(__dirname, 'dist/Ascii')));

let server = http.createServer(app);
server.on('listening', () => {
    console.log("Listening on :" + port);
    fs.rmdirSync(path.join(__dirname, "src/backend/uploads"), {recursive: true})
});
server.listen(port);

