import {HttpClient} from "@angular/common/http";
import asciidoctorType from "asciidoctor";
import {HighlightJS} from "ngx-highlightjs";

export class Page {
    contentHTML: HTMLElement;
    speakerNotes: Array<HTMLElement>;

    constructor(contentHtml: HTMLElement, speakerNotes: Array<HTMLElement>) {
        this.contentHTML = contentHtml;
        this.speakerNotes = speakerNotes;
    }

    speakerNotesText() {
        let returnText = "";
        for (const note of this.speakerNotes) {
            Array.from(note.querySelectorAll("p")).forEach(
                (p_element) => {
                    returnText += p_element.outerHTML;
                }
            )
        }
        return returnText == "" ? "<p>No Notes.</p>" : returnText;
    }
}

declare const Asciidoctor: typeof asciidoctorType;

export class AsciiDocParser {
    rawAsciiDoc: string = "";
    rawAsciiUrl: string;
    asciiDoc = Asciidoctor();
    http: HttpClient;
    hljs: HighlightJS;

    constructor(rawAsciiUrl: string, httpClient: HttpClient, highlightJS: HighlightJS) {
        this.rawAsciiUrl = rawAsciiUrl;
        this.http = httpClient;
        this.hljs = highlightJS;
    }

    async parse(): Promise<Array<Page>> {
        this.rawAsciiDoc = await this.http.get(this.rawAsciiUrl, {responseType: "text"}).toPromise();
        let paginator = Array<Page>();
        let document = this.asciiDoc.convert(this.rawAsciiDoc);
        let htmlDocument = new DOMParser().parseFromString(document.toString(), 'text/html');
        let includeObjects = Array.from(htmlDocument.documentElement.querySelectorAll(
            "p > a.bare"
        ));
        for (const includeObject of includeObjects) {
            let result = await this.http.get(includeObject.innerHTML, {responseType: "text"}).toPromise();
            includeObject.innerHTML = this.asciiDoc.convert(
                result
            ).toString();
        }
        htmlDocument.documentElement.querySelectorAll(".sect0, .sect1, .sect2").forEach((elem) => {
            let copy = elem.cloneNode(true) as HTMLElement;
            copy.querySelectorAll('.sect0, .sect1, .sect2').forEach((subElement) => {
                subElement.outerHTML = null;
            })
            copy.querySelectorAll("pre code").forEach((block) =>
                this.hljs.highlightBlock(block as HTMLElement).subscribe());
            let speakerNotes = Array<HTMLElement>();
            for (const speakerNote of Array.from(copy.querySelectorAll(".admonitionblock.note.speaker"))) {
                speakerNotes.push(speakerNote.cloneNode(true) as HTMLElement);
                speakerNote.outerHTML = "";
            }
            paginator.push(new Page(copy, speakerNotes));
        })
        return paginator;
    }
}
