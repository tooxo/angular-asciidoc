import {Component, OnInit, ChangeDetectorRef, HostListener} from '@angular/core';
import {AsciiDocParser, Page} from "./AsciiDocParser";
import {HttpClient} from "@angular/common/http";
import {HighlightJS} from "ngx-highlightjs";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    _paginator = Array<Page>();
    filename = "test.adoc";
    index = 11;

    http;
    highlightJs;

    constructor(httpClient: HttpClient, highlightJS: HighlightJS, private cdRef: ChangeDetectorRef) {
        this.http = httpClient;
        this.highlightJs = highlightJS;
    }

    @HostListener("document:keydown", ["$event"])
    keyPressed(event: KeyboardEvent) {
        switch (event.key) {
            case "ArrowLeft":
                this.index --;
                break;
            case "ArrowRight":
                this.index++;
                break;
        }
    }


    ngOnInit() {
        new AsciiDocParser(this.filename, this.http, this.highlightJs).parse().then((result) => {
                this._paginator = result;
                this.cdRef.detectChanges();
            }
        );
    }
}
