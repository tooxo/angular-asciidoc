import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {AsciiDisplayComponent} from "./ascii-display/ascii-display.component";
import { AsciiNextComponent } from './ascii-next/ascii-next.component';
import { AsciiNotesComponent } from './ascii-notes/ascii-notes.component';

const appRoutes: Routes = [
    {
        path: '',
        component: AppComponent
    }
];

@NgModule({
    declarations: [
        AppComponent,
        AsciiDisplayComponent,
        AsciiNextComponent,
        AsciiNotesComponent
    ],
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true}
        ),
        BrowserModule,
        FormsModule,
        HttpClientModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
