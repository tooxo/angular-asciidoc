import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsciiDisplayComponent } from './ascii-display.component';

describe('AsciiDisplayComponent', () => {
  let component: AsciiDisplayComponent;
  let fixture: ComponentFixture<AsciiDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsciiDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsciiDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
