import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Page} from '../AsciiDocParser'

@Component({
    selector: 'app-ascii-display',
    templateUrl: './ascii-display.component.html',
    styleUrls: ['./ascii-display.component.css'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class AsciiDisplayComponent implements OnInit {
    rendered_html: string;
    _paginator: Array<Page>;
    _index: number = 0;

    nextPage(): void {
        if (this._index == this._paginator.length - 1 || this._paginator.length == 0) {
            this._index = 0;
        } else {
            this._index++;
        }
        this.rendered_html = this._paginator[this._index].contentHTML.outerHTML;
    }

    previousPage(): void {
        if (this._paginator.length == 0) {
            return;
        }
        if (this._index == 0) {
            this._index = this._paginator.length - 1;
        } else {
            this._index--;
        }
        this.rendered_html = this._paginator[this._index].contentHTML.outerHTML;
    }

    goToPage(page: number): void {
        if (this._paginator.length < page) {
            return;
        }
        this._index = page - 1;
        this.rendered_html = this._paginator[this._index].contentHTML.outerHTML;
    }

    @Input()
    set paginator(paginator: Array<Page>) {
        console.log("new paginator", paginator);
        this._paginator = paginator;
        this.goToPage(this._index + 1);
    }

    @Input()
    set index(index: number) {
        this._index = index;
        this.goToPage(this._index + 1);
    }

    constructor() {
    }

    ngOnInit(): void {
    }
}
