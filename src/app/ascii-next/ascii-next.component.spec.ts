import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsciiNextComponent } from './ascii-next.component';

describe('AsciiNextComponent', () => {
  let component: AsciiNextComponent;
  let fixture: ComponentFixture<AsciiNextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsciiNextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsciiNextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
