import {Component, Input, OnInit, Output} from '@angular/core';
import {Page} from '../AsciiDocParser';

@Component({
    selector: 'app-ascii-next',
    templateUrl: './ascii-next.component.html',
    styleUrls: ['./ascii-next.component.css']
})
export class AsciiNextComponent implements OnInit {
    _paginator: Array<Page>;
    _index: number = 0;

    @Input()
    set index(index: number) {
        this._index = index;
    }

    @Input()
    set paginator(paginator: Array<Page>) {
        this._paginator = paginator;
    }

    @Output()
    get get_index() {
        return this._index + 1;
    }

    constructor() {
    }

    ngOnInit(): void {
    }

}
