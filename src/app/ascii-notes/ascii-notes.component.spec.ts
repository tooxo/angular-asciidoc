import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsciiNotesComponent } from './ascii-notes.component';

describe('AsciiNotesComponent', () => {
  let component: AsciiNotesComponent;
  let fixture: ComponentFixture<AsciiNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsciiNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsciiNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
