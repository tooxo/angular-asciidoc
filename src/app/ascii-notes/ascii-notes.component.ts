import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Page} from "../AsciiDocParser";

@Component({
    selector: 'app-ascii-notes',
    templateUrl: './ascii-notes.component.html',
    styleUrls: ['./ascii-notes.component.css'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class AsciiNotesComponent implements OnInit {

    _paginator: Array<Page>;
    _index: number;

    @Input()
    set paginator(paginator: Array<Page>) {
        this._paginator = paginator;
    }

    @Input()
    set index(index: number) {
        this._index = index;
    }

    constructor() {
    }

    ngOnInit(): void {
    }

}
