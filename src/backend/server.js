const path = require("path");
const express = require("express");
const fileUpload = require('express-fileupload');
const cors = require('cors');
const app = express();
let fs = require("fs");
let cookieParser = require('cookie-parser')

app.use(
    fileUpload({
        createParentPath: true
    })
)

app.use(cors());
app.use(cookieParser());

app.get("/*", function (req, res, next) {
    if (!req.cookies.uuid) {
        // Need to provide UUID to request the correct files
        next();
    } else {
        let proposedFilename = path.join(__dirname, "/uploads/", req.cookies.uuid, req.params[0]);
        if (fs.existsSync(proposedFilename)) {
            res.sendFile(proposedFilename);
        } else {
            next();
        }
    }
})

app.post("/upload/", async (req, res) => {
    console.log("Upload started");
    if (!req.files || !req.query.id) {
        return res.sendStatus(400);
    }
    Object.keys(req.files).forEach(function (key) {
        let file = req.files[key];
        file.mv(__dirname + "/uploads/" + req.query.id + "/" + file.name)
    })
    return res.send({
        ok: true,
        code: 200
    });
})

module.exports = app;
